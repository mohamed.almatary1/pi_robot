#Instructions 
- move the server script to the raspberry that will host the server
- Install and configure DHCP on that raspberry/PC, also configure a wifi endpoint
- Get the raspberry IP adress after the DHPC config is done, we'll call it server_ip
- change the the HOST_SERVER field in both of the robot and controller scripts to server_ip
- copy the robot script into the raspberry of the robot
- copy the controller script into a different raspberry or PC that represents the controller
- start the dhcp server on the host raspberry then start the access endpoint using the commands bellow : 


        #start service dhcp
        sudo service isc-dhcp-server start

        #verify the status of  service dhcp
        sudo service isc-dhcp-server status

        # start access point
        sudo hostapd /etc/hostapd/hostapd.conf


- connect the robot and the controller to the endpoint
- on the server raspberry launch the server script
- on the robot raspberry launch the robot script, the server should be notified
- on the controller machine launch the controller script, the server should be notified
- on the controller machine use the arrow keys to controll the movement of the car



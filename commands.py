import RPi.GPIO as GPIO
import time
import sys

# 40 GPIO21 avant
# 38 GPIO20 droite
# 37 GPIO26 gauche
# 35 GPIO19 arriere






def prepare():
    
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(40,GPIO.OUT)
    GPIO.setup(38,GPIO.OUT)
    GPIO.setup(37,GPIO.OUT)
    GPIO.setup(35,GPIO.OUT)


def forward():
    p=GPIO.PWM(40,1)
    p.start(100)
    time.sleep(10)
    p.stop()
    #GPIO.cleanup()
    
def right():
     
    p=GPIO.PWM(38,0.5)
    p.start(100)
    time.sleep(2)
    p.stop()
    #GPIO.cleanup()
    
def left():
    p=GPIO.PWM(37,0.5)
    p.start(100)
    time.sleep(2)
    p.stop()
    #GPIO.cleanup()
    
def backward():
    p=GPIO.PWM(35,1)
    p.start(100)
    time.sleep(10)
    p.stop()
    #GPIO.cleanup()
    
def cleanup():
    GPIO.cleanup()


     
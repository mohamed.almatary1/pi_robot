import socket
import pynput
import keyboard


commands = { "haut" :"forward", "bas" : "backward", "gauche" : "left", "droite" :"right"}
HOST_SERVER = '192.168.0.1'
#HOST_SERVER = '10.42.0.137'


PORT_SERVER = 9000

#HOST=''
PORT=''

serv = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
#serv.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
serv.connect((HOST_SERVER,PORT_SERVER))
serv.send('connect ILMI_ROBOT'.encode())

message = serv.recv(1024).decode()
print(message)
if message == "ok":

    while True:
        e = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        e.connect((HOST_SERVER, PORT_SERVER))
        s = keyboard.read_key()
        if s in commands:
            e.send(commands[s].encode())
        if s == "esc":
            break
        e.close()

serv.close()